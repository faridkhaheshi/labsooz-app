import React from 'react';
import { View, I18nManager, StatusBar, Image, Text } from 'react-native';

// import FlashMessage from 'react-native-flash-message';
import { SplashScreen, AppLoading } from 'expo';
import { Root } from "native-base";
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import constants from './src/constants';
import AppContainer from './src/navigators/app.container';
import styles from './src/styles/styles';
import { MenuProvider } from 'react-native-popup-menu';
import { logout } from './src/services/auth.service';
import { translateForUser } from './src/services/error.service';

I18nManager.forceRTL(true);

export default class App extends React.Component{
	constructor(props){
		super(props);
		this._cacheSplashResourcesAsync = this._cacheSplashResourcesAsync.bind(this);
		this._cacheResourcesAsync = this._cacheResourcesAsync.bind(this);
		this._setuserInfo = this._setuserInfo.bind(this);
		this._setErrorMessage = this._setErrorMessage.bind(this);
		this._markSplashReady = this._markSplashReady.bind(this);
		this._handleSplashError = this._handleSplashError.bind(this);
	}

	state = {
		isSplashReady: false,
		isAppReady: false,
		minSplashTimePassed: false,
		userInfo: null,
		errorMessage: '',
	};

	componentDidMount = () => {
		// ensuring that the splash screen is shown for at least 3 seconds.
		setTimeout(() => { this.setState({ minSplashTimePassed: true }) }, constants.timing.minSplash);
	}
	
	_setuserInfo = userInfo => {
		this.setState({ userInfo });
	}

	_logout = logout;
	
	_setErrorMessage = text => this.setState({errorMessage: text});

	_markSplashReady = () => this.setState({ isSplashReady: true });

	_handleSplashError = error => {
		this._setErrorMessage(translateForUser(error));
	};

	render = () => {
		// Here, we first load all resources before closing the splash screen. Then the app is loaded.
		if(!this.state.isSplashReady){
			return(
				<AppLoading
					startAsync={ this._cacheSplashResourcesAsync }
					onFinish={ this._markSplashReady }
					onError={ this._handleSplashError }
					autoHideSplash={ false }
				/>
			);
		}


		if(!this.state.isAppReady || !this.state.minSplashTimePassed){
			// This is the Splash Screen
			return(
				<View style={ styles.splashContainer }>
					<StatusBar hidden={true}/>
					<Image 
						style={ styles.splashImage }
						source={ require('./assets/images/splash.png')} 
						onLoad={ this._cacheResourcesAsync }
					/>
					<View style={styles.splashMessage}>
						<Text style={styles.splashMessageText}>{this.state.errorMessage}</Text>
					</View>
				</View>
			);
		}
		

		return (
			<MenuProvider>
				<Root>
					<AppContainer screenProps={
						{
						userInfo: this.state.userInfo,
						setUserInfo: this._setuserInfo,
						actions: { logout: this._logout },
						}
					}/>
				</Root>
			</MenuProvider>
		);
	}

	_cacheSplashResourcesAsync = async () => {
		try{
			const splashPng = require('./assets/images/splash.png');
			await Asset.fromModule(splashPng).downloadAsync();
		}
		catch(error){
			throw new DeviceError({code: 2001, message: error.message || 'some error happened at _cacheSplashResourcesAsync.app'});
		}
	};

	_cacheResourcesAsync = async () => {
		try{
			SplashScreen.hide();
			const images = [
				// require('./assets/images/some-image.png'),
			];
			const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
			const fontsLoaded = Font.loadAsync({
				'iran-sans': require('./assets/fonts/IRANSansMobile.ttf'),
				'iran-sans-bold': require('./assets/fonts/IRANSansMobile_Bold.ttf'),
				Roboto: require("native-base/Fonts/Roboto.ttf"),
				Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
				// Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
			});

			await Promise.all([ ...cacheImages, fontsLoaded ]);
			this.setState({ isAppReady: true });
		}
		catch(error){
			this._setErrorMessage(translateForUser(error));
		}
	};
};