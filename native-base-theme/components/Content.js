// @flow

export default () => {
  const contentTheme = {
    flex: 1,
    marginHorizontal: 10,     // Added by me
    marginVertical: 5,        // Added by me
    backgroundColor: 'transparent',
    'NativeBase.Segment': {
      borderWidth: 0,
      backgroundColor: 'transparent'
    },
  };

  return contentTheme;
};
