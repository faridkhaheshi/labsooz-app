import { StyleSheet, Dimensions } from 'react-native';
const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default StyleSheet.create({
	closeButton: {
		width: 60,
		height: 60,
		position: 'absolute',
		top: 30,
		left: 20,
	},
    preview: {
		height: winHeight,
		width: winWidth,
		position: 'absolute', 
		left: 0,
		top: 0,
		right: 0,
		bottom: 0,
	},
	permissionContentStyle: {
		flex: 1, 
		alignItems: 'center', 
		justifyContent: 'center'
	},
});