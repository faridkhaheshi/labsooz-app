import React from 'react';
import { 
    View,
    ActivityIndicator } from 'react-native';
import styles from '../styles/styles';
import { checkSignIn } from '../services/auth.service';
import { translateForUser } from '../services/error.service';
import { showError } from '../services/toast.service';

export default class AppLoadingPage extends React.Component{
	constructor(props){
		super(props);
	}

	state = { checkedSignIn: false };

	componentDidMount = async () => {
		try{
			const userInfo = await checkSignIn();
			if(userInfo) this.props.screenProps.setUserInfo(userInfo);
			else this.setState({ checkedSignIn: true });	
		}
		catch(error){
			showError(translateForUser(error));
		}
    }
    
    componentDidUpdate = (prevProps, prevStates) => {
		if(this.props.screenProps.userInfo || this.state.checkedSignIn)
			this.props.navigation.navigate(this.props.screenProps.userInfo?'App':'Auth');
    };

	render(){
        
        return (
            <View style={{flex: 1, justifyContent: 'center'}}>
              <ActivityIndicator size="large" color="#f4511e"/>
            </View>
          );
	}
};