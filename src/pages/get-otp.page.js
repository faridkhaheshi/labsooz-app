import React from 'react';
import { 
    KeyboardAvoidingView,  
    View, 
 } from 'react-native';
import { 
    Container, 
    Content, 
    Card, 
    CardItem,
    Body, 
    Form,
    Item,
    Input,
    StyleProvider, 
    Icon, 
    Text 
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';
import MyHeader from '../components/my-header.component';
import MyBottomButton from '../components/my-bottom-button.component';
import MyCardItem from '../components/my-card-item.component';
import { getOtp } from '../services/auth.service';
import PhoneNumberInput from '../components/phone-number-input.component';
import styles from '../styles/styles';
import constants from '../constants';
import tools from '../utils/tools';
import { translateForUser } from '../services/error.service';
import { showError } from '../services/toast.service';

export default class GetOtpPage extends React.Component {

    state = {
        buttonActive: false,
        mobileNumber: null,
        processing: false,
    };

    pageTexts = {
        title: constants.appTexts.login.getOtpTitle,
        guide: constants.appTexts.login.getOtpGuide,
        phoneNumberPlaceholder: constants.appTexts.home.phoneNumberPlaceholder,
        button: constants.appTexts.login.confirmPhoneNumber,
    };


    _handleChangeText = text => {
        try{
            const normalizedPhoneNumber = tools.string.normalizePhoneNumber(text);
            this.setState({ buttonActive: true, mobileNumber: normalizedPhoneNumber });
        }
        catch(error){
            this.setState({ buttonActive: false, mobileNumber: null });
        }
    };

    _resetPage = () => this.setState({ processing: false });

    _handleSubmit = async () => {
        try{
            this.setState({ processing: true });
            const otpData = await getOtp(this.state.mobileNumber);
            this.setState({ processing: false });
            this.props.navigation.navigate('Login', {
                mobileNumber: this.state.mobileNumber,
                expiresInSeconds: otpData.expiresInSeconds,
            });
        }
        catch(error){
            showError(translateForUser(error));
            this._resetPage();
        }
    };

    render = () => {
        return (
            <StyleProvider style={getTheme(labsoozTheme)}>
                <Container>
                    <MyHeader title={ this.pageTexts.title } noMenu />
                    <KeyboardAvoidingView style={{backgroundColor: 'transparent', flex: 1}} behavior='padding' enabled>
                        <Content contentContainerStyle={{ flex: 1 }} enableAutomaticScroll={ false }>
                            <View style={styles.topContainer}>
                                <Card>
                                    <MyCardItem type='text' text={ this.pageTexts.guide }/>
                                    <MyCardItem type='mobile' 
                                        text={ this.pageTexts.guide }
                                        getUpdates={ this._handleChangeText } 
                                        formSuccess={ this.state.buttonActive }
                                        autoFocus={ true }/>
                                </Card>
                            </View>
                            <MyBottomButton 
                                text={ this.pageTexts.button } 
                                onPress={ this._handleSubmit }
                                disabled={ !this.state.buttonActive }
                                processing = { this.state.processing }
                                />
                        </Content>
                    </KeyboardAvoidingView>
                </Container>
            </StyleProvider>
        );
    };
};