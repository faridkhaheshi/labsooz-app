import React from 'react';
import { View } from 'react-native';
import { 
    Container, 
    Content, 
    Card,
    StyleProvider,   
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';
import constants from '../constants';
import styles from '../styles/styles';
import MyHeader from '../components/my-header.component';
import MyBottomButton from '../components/my-bottom-button.component';
import MyCardItem from '../components/my-card-item.component';



export default class HomePage extends React.Component {
    // We have userInfo @ this.props.screenProps.userInfo

    constructor(props){
        super(props);
        this.openCamera = this.openCamera.bind(this);
    }


    pageTexts = {
        title: constants.appTexts.home.title,
        guide: constants.appTexts.home.noPhotoGuide,
        button: constants.appTexts.home.goToCameraButton,
    };


    shouldComponentUpdate = (nextProps, nextStates) => false;

    openCamera = () => this.props.navigation.navigate('Camera');
    
    render = () => {
        return(
            <StyleProvider style={getTheme(labsoozTheme)}>
                <Container>
                    <MyHeader
                        title={ this.pageTexts.title }
                        navigation={ this.props.navigation } 
                        appActions={ this.props.screenProps.actions } />
                    <Content contentContainerStyle={{ flex: 1 }}>
                        <View style={styles.topContainer}>
                            <Card>
                                <MyCardItem type='text' text={this.pageTexts.guide}/>
                            </Card>
                        </View>
                        <MyBottomButton text={ this.pageTexts.button } onPress={ this.openCamera }/>
                    </Content>
                </Container>
            </StyleProvider>
        );
    };
}