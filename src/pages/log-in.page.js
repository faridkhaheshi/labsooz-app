import React from 'react';
import { 
    KeyboardAvoidingView,  
    View,
 } from 'react-native';
import { 
    Container, 
    Content, 
    Card, 
    CardItem,
    Body, 
    Form,
    Item,
    Input,
    StyleProvider, 
    Icon, 
    Text 
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';
import MyHeader from '../components/my-header.component';
import MyBottomButton from '../components/my-bottom-button.component';
import MyCardItem from '../components/my-card-item.component';

import { login } from '../services/auth.service';
import { translateForUser } from '../services/error.service';
import { showError } from '../services/toast.service';
import styles from '../styles/styles';
import constants from '../constants';
import tools from '../utils/tools';

export default class LoginPage extends React.Component {

    state = {
        buttonActive: false,
        processing: false,
        otp: null,
    };

    pageTexts = {
        title: constants.appTexts.login.loginTitle,
        guide: constants.appTexts.login.loginGuide,
        otpPlaceholder: constants.appTexts.login.otpPlaceholder,
        button: constants.appTexts.login.confirmOtp,
    };

    _handleChangeText = text => {
        try{
            const normalizedOtp = tools.string.normalizeOtp(text);
            this.setState({ buttonActive: true, otp: normalizedOtp });
        }
        catch(error){
            this.setState({ buttonActive: false, otp: null });
        }
    };
    _resetPage = () => this.setState({ processing: false });

    login = async () => {
        try{
            this.setState({ processing: true });
            const { params: navParams } = this.props.navigation.state;
            const mobileNumber = (navParams || {}).mobileNumber;
            const userInfo = await login({
                mobileNumber, 
                otp: this.state.otp 
            });
           
            this.props.screenProps.setUserInfo(userInfo);
            this.setState({ processing: false });
            this.props.navigation.navigate( 'App' );
        }
        catch(error){
            showError(translateForUser(error));
            this._resetPage();
        }
    };

    render = () => {
        const { params: navParams } = this.props.navigation.state;
        const mobileNumber = (navParams || {}).mobileNumber;
        const expiresInSeconds = (navParams || {}).expiresInSeconds;
        return (
            <StyleProvider style={getTheme(labsoozTheme)}>
                <Container>
                    <MyHeader title={ this.pageTexts.title } noMenu />
                    <KeyboardAvoidingView style={{backgroundColor: 'transparent', flex: 1}} behavior='padding' enabled>
                        <Content contentContainerStyle={{ flex: 1 }} enableAutomaticScroll={ false }>
                            <View style={styles.topContainer}>
                                <Card>
                                    <MyCardItem type='text' text={ this.pageTexts.guide }/>
                                    <MyCardItem type='input'  
                                        placeholder={ this.pageTexts.otpPlaceholder } 
                                        maxLength={ constants.appParams.otpMaxLen }
                                        keyboardType='numeric'
                                        autoFocus={ true }
                                        getUpdates={ this._handleChangeText } />
                                </Card>
                            </View>
                            <MyBottomButton 
                                text={ this.pageTexts.button } 
                                onPress={ this.login }
                                disabled={ !this.state.buttonActive }
                                processing = { this.state.processing }
                                />
                        </Content>
                    </KeyboardAvoidingView>
                </Container>
            </StyleProvider>
        );
    };
};