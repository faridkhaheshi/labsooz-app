import { Platform, StatusBar } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import CameraPage from './camera.page';
import HomePage from './home.page';
import ResultFormPage from './result-form.page';
import ConfirmResultPage from './confirm-result.page';

const AppNavigator = createStackNavigator({
    Home: { screen: HomePage },
    Camera: { screen: CameraPage },
    ResultForm: { screen: ResultFormPage },
    ConfirmResult: { screen: ConfirmResultPage },
},
    {
        initialRouteName: "Home",
        cardStyle: {
            paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
        },
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontSize: 18,
                fontFamily: 'iran-sans-bold',
                writingDirection: 'rtl',
                textAlign: 'justify',
            },
            gestureDirection: 'inverted',
        },
    }
);

export default createAppContainer(AppNavigator);
