import React from 'react';
import {
        Image, 
        View, 
} from 'react-native';

import { 
    Container, 
    Content, 
    Card, 
    Body,
    StyleProvider, 
    Text 
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';

import MyHeader from '../components/my-header.component';
import MyBottomButton from '../components/my-bottom-button.component';
import MyCardItem from '../components/my-card-item.component';
import styles from '../styles/styles';
import constants from '../constants';
import tools from '../utils/tools';

export default class ResultFormPage extends React.Component {
    // We have userInfo @ this.props.screenProps.userInfo

    state = {
        phoneNumber: null,
        buttonActive: false,
    };

    pageTexts = {
        title: constants.appTexts.resultForm.title,
        button: constants.appTexts.home.confirmPhoneNumber,
        imageDescription: constants.appTexts.resultForm.imageDescription,
        guide: constants.appTexts.resultForm.enterPhoneNumberGuide,
        phoneNumberPlaceholder: constants.appTexts.home.phoneNumberPlaceholder,
    };

    // This method submits the result out of the component
    _handleSubmit = () => {
        const { params: navParams } = this.props.navigation.state;
        this.props.navigation.navigate('ConfirmResult', {
            result: {
                clientMobileNumber: this.state.phoneNumber,
                localPhotoUri: navParams.localPhotoUri,
            }
        });
    };

    // This method checks the phone number to ensure that it is valid.
    _handleChangeText = (text) => {
        try{
            const normalizedPhoneNumber = tools.string.normalizePhoneNumber(text);
            this.setState({ buttonActive: true, phoneNumber: normalizedPhoneNumber });
        }
        catch(error){
            this.setState({ buttonActive: false, phoneNumber: null });
        }
    };

    render = () => {
        const { params: navParams } = this.props.navigation.state;
        

        return(
            <StyleProvider style={getTheme(labsoozTheme)}>
                <Container>
                    <MyHeader hasClose
                        title={ this.pageTexts.title }
                        navigation={ this.props.navigation } 
                        appActions={ this.props.screenProps.actions } />
                    <Content viewIsInsideTabBar={true} extraScrollHeight={100} enableOnAndroid={true}>
                        <View style={styles.topContainer}>
                            <Card>
                                <MyCardItem bordered type='image-text' 
                                    image={{ uri: navParams.localPhotoUri }} 
                                    text={ this.pageTexts.imageDescription } />
                                <MyCardItem type='text-mobile'  
                                    text={ this.pageTexts.guide } 
                                    getUpdates={ this._handleChangeText } 
                                    formSuccess={ this.state.buttonActive }
                                    autoFocus={ true }/>
                            </Card>
                        </View>
                        <MyBottomButton 
                            text={ this.pageTexts.button } 
                            onPress={ this._handleSubmit }
                            disabled={ !this.state.buttonActive }
                            />
                    </Content>
                </Container>
            </StyleProvider>
        );
    }
};