import React from 'react';
import { View } from 'react-native';
import { 
    Container, 
    Content, 
    Card, 
    CardItem,
    Body, 
    StyleProvider, 
    Text,
} from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';
import MyHeader from '../components/my-header.component';
import MyBottomButton from '../components/my-bottom-button.component';
import MyCardItem from '../components/my-card-item.component';
import styles from '../styles/styles';
import constants from '../constants';
import tools from '../utils/tools';
import ImageUploader from '../components/image-uploader.component';
import { addResult } from '../services/result.service';
import { showToast, showError, showSuccess, showInfo } from '../services/toast.service';
import { translateForUser } from '../services/error.service';

const defaultMobileNumber = '999999';
const defaultPhotoUri = 'http://random.com';

/*
 The main job of this component is to submit the new result to the server after the user confirms it.
 It does the following jobs:
    1. Gets the confirmation from user.
    2. Uploads the image to the server.
    3. submits the result to the server.
    4. Informs its parent that the job is done.
*/
export default class ConfirmResultPage extends React.Component {
     // We have userInfo @ this.props.screenProps.userInfo
    constructor(props){
        super(props);
        this.handleJobDone = this.handleJobDone.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    imageUploader = null;

    registerImageUploader = ref => this.imageUploader = ref;

    state = { 
        buttonEnabled: true,
    };

    pageTexts = {
        title: constants.appTexts.confirmResult.title,
        button: constants.appTexts.confirmResult.button,
        imageDescription: constants.appTexts.resultForm.imageDescription,
        guide: constants.appTexts.confirmResult.guide,
        phoneNumberPlaceholder: constants.appTexts.home.phoneNumberPlaceholder,
        resultAddedToast: constants.appTexts.confirmResult.resultAddedMessage,
        uploadStartedToast: constants.appTexts.confirmResult.uploadStartMessage,
        uploadFinishedToast: constants.appTexts.confirmResult.uploadFinishedMessage,
    };

    resetState = () => this.setState({ buttonEnabled: true });

    componentDidMount = () => {};

    componentDidUpdate = (prevProps, prevState) => {};

    // This is called when the job is done and the result is received from server.
    handleJobDone = serverResult => {
        showSuccess(this.pageTexts.resultAddedToast);
        setTimeout(() => {
            this.resetState();
            this.imageUploader.resetProgress();
            this.props.navigation.navigate('Home');
        }, constants.timing.goToNextResult );
    };

    // When the user confirms the phone number, we start to upload the image.
    handleConfirm = async () => {
        const { params: navParams } = this.props.navigation.state;
        const clientMobileNumber = ((navParams || {}).result || {}).clientMobileNumber || defaultMobileNumber;
        
        this.setState({ buttonEnabled: false });
        showInfo(this.pageTexts.uploadStartedToast);
        try{
            const publicUrl = await this.imageUploader.upload();
            showSuccess(this.pageTexts.uploadFinishedToast);
            const result = { photoUrl: publicUrl, clientMobileNumber };
            const serverResult = await addResult({ 
                result,
                userInfo: this.props.screenProps.userInfo,
            });
            this.handleJobDone(serverResult);
        }
        catch(error){
            showError(translateForUser(error));
            this.resetState();
            this.imageUploader.resetProgress();
        }
    };

    render(){
        const { params: navParams } = this.props.navigation.state;
        const localPhotoUri = ((navParams || {}).result || {}).localPhotoUri || defaultPhotoUri;
        const clientMobileNumber = ((navParams || {}).result || {}).clientMobileNumber || defaultMobileNumber;
        
        return (
            <StyleProvider style={getTheme(labsoozTheme)}>
                <Container>
                    <MyHeader hasClose
                        title={ this.pageTexts.title }
                        navigation={ this.props.navigation } 
                        appActions={ this.props.screenProps.actions } />
                    <Content contentContainerStyle={{ flex:1 }}>
                        <View style={styles.topContainer}>
                            <Card>
                                <MyCardItem type='text' text={ this.pageTexts.guide }/>
                                <MyCardItem bordered type='text' 
                                    bodyStyle={ { alignItems: 'center' } }
                                    textStyle={ styles.bigText }
                                    text={ tools.string.polishPhoneNumber(clientMobileNumber) }/>

                                <CardItem bordered>
                                    <ImageUploader
                                        style={ styles.resultPhoto } 
                                        localPhotoUri={ localPhotoUri } 
                                        ref={ this.registerImageUploader }
                                        userInfo={ this.props.screenProps.userInfo }
                                    />
                                </CardItem>
                            </Card>
                        </View>
                        <MyBottomButton 
                            text={ this.pageTexts.button } 
                            onPress={ this.handleConfirm }
                            disabled={ !this.state.buttonEnabled }
                            processing = { !this.state.buttonEnabled }
                            />
                    </Content>
                </Container>
            </StyleProvider>
        );
    }
};