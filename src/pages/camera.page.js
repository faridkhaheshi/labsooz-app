import React from 'react';
import { View } from 'react-native';
import { 
    Container, 
    Content,  
    Right, 
    StyleProvider, 
    Button, 
	Icon,
	Text,
} from 'native-base';

import { Ionicons } from '@expo/vector-icons';
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';
import { NavigationEvents } from 'react-navigation';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';
import Toolbar from '../components/toolbar.component';
import styles from './styles.camera.page';
import constants from '../constants';
import { translateForUser } from '../services/error.service';
import { showError } from '../services/toast.service';

export default class CameraPage extends React.Component {
	// We have userInfo @ this.props.screenProps.userInfo
	camera = null;

	state = {
		cameraEnabled: true,
		flashMode: Camera.Constants.FlashMode.off,
		capturing: null,
		cameraType: Camera.Constants.Type.back,
		hasCameraPermission: null,
	};

	pageTexts = {
		givePermissionButton: constants.appTexts.camera.givePermissionButton,
		givePermissionGuide: constants.appTexts.camera.givePermissionGuide,
	};

	setFlashMode = flashMode => this.setState({ flashMode });
	setCameraType = cameraType => this.setState({ cameraType });

	closeCamera = () => this.props.navigation.navigate('Home');

	handleCapture = async () => {
		try{
			this.setState({ capturing: true });
			const photoData = await this.camera.takePictureAsync();
			this.setState({ capturing: false });
			
			this.props.navigation.navigate('ResultForm', { localPhotoUri: photoData.uri });
		}
		catch(error){
			showError(translateForUser(error));
		}
		
	};

	componentDidMount() {
		this.askPermission();
	}

	
	askPermission = async () => {
		const camera = await Permissions.askAsync(Permissions.CAMERA);
		const hasCameraPermission = (camera.status === 'granted');
		this.setState({ hasCameraPermission });
	};

	enableCamera = () => this.setState({ cameraEnabled: true });
	disableCamera = () => this.setState({ cameraEnabled: false });
	registerCamera = cameraRef => this.camera = cameraRef;

	render(){
		const { hasCameraPermission, flashMode, cameraType, capturing, captures } = this.state;


		if(hasCameraPermission === null){
			return <View />;
		}
		else if(hasCameraPermission === false){
			return (
				<StyleProvider style={getTheme(labsoozTheme)}>
					<Container>
						<Content contentContainerStyle={ styles.permissionContentStyle }>
							<Text>{ this.pageTexts.givePermissionGuide }</Text>
							<Button style={{ marginTop: 30 }} onPress={ this.askPermission }>
								<Text>{ this.pageTexts.givePermissionButton }</Text>
							</Button>
						</Content>
					</Container>
				</StyleProvider>
			);
		}
		return(
			<StyleProvider style={getTheme(labsoozTheme)}>
                <Container>
					<View>
						<NavigationEvents
							onWillFocus={ this.enableCamera }
							onDidBlur={ this.disableCamera }
						/>
						{this.state.cameraEnabled && (
						<Camera
							type={ cameraType }
							flashMode = { flashMode }
							style={ styles.preview }
							ref={ this.registerCamera }
						/>
						)}
					</View>
					<Button transparent style={styles.closeButton} onPress={ this.closeCamera }>
						<Ionicons
							name="md-close"
							color="white"
							size={30}
						/>
					</Button>
					<Toolbar
						camera={ this.camera }
						capturing={ capturing }
						cameraType={ cameraType }
						flashMode={ flashMode }
						setFlashMode={ this.setFlashMode }
						setCameraType={ this.setCameraType }
						onCapture={ this.handleCapture }
					/>
				</Container>
			</StyleProvider>
		);
	}
};