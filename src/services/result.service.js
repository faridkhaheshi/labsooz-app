import React from 'react';
import constants from '../constants';
import api from './api.service';

const addResult = async ({ result, userInfo }) => {
    try{
        const theResult = await api.addResult({ result, userInfo });
        return theResult;
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in addResult.result.service'});
    }
};

export { addResult };