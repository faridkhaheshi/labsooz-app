import { errorTypes, errorCodes } from '../utils/errors';


const translateForUser = error => {
    try{
        return errorCodes[error.code]['toUser'];
    }
    catch(error){
        return errorCodes[9999]['toUser'];
    }
};

export { translateForUser };