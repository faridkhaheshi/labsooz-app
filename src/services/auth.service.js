import { AsyncStorage } from 'react-native';
import constants from '../constants';
import api from './api.service';
import { UnknownError, DeviceError } from '../utils/my-errors';


const userInfoKey = constants.appParams.localStorageKey;

/*
    This method asks the api for a new one-time password for the given mobile number.
    If the request is successful, this method will resolves with the data about the one time password.
    At this moment, this data only includes a property called "expiresInSeconds".
    {
        expiresInSeconds: 600
    }
    If the request fails for any reason, This method rejects with an Error. The error is one of 
    MyErro s. it will include the error type and error code along with the usual error message.
*/
const getOtp = async mobileNumber => {
    try{
        const otpData = await api.getOtp(mobileNumber);
        return otpData;
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in getOtp.auth.service'});
    }
}

/*
    This method is responsible for logging in the user. In response, it will resolve with
    the userInfo. 
    If the process fails for any reason, this method will reject with an appropriate error.
    The error will including an error code with appropriate error type and message.
    In case of a ServerError, the error will also include the http status.
*/
const login = async ({ mobileNumber, otp }) => {
    try{
        const userInfo = await api.login({ mobileNumber, otp });
        await AsyncStorage.setItem(userInfoKey, JSON.stringify(userInfo));
        return userInfo;
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in login.auth.service'});
    }
};

const logout = async () => {
    try{
        await AsyncStorage.removeItem(userInfoKey);
    }
    catch(error){
        throw new DeviceError({ code: 1003 , message: error.message || 'Error happened when trying to remove user info from device storage' });
    }
};

const checkSignIn = async () => {
    try{
        // await logout();
        const userInfoStr = await AsyncStorage.getItem(userInfoKey);
        const userInfo = JSON.parse(userInfoStr);
        return userInfo;
    }
    catch(error){
        throw new DeviceError({ code: 1002 , message: error.message || 'Error happened when trying to retrieve user info from device storage' });
    }
};

export { checkSignIn, getOtp, login, logout };