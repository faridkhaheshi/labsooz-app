import constants from '../constants';
import { ServerError, DeviceError } from '../utils/my-errors';



const getUploadPermission = async userInfo => {

    // TODO: change this method to get the data from server.

    try{
        const { token, primaryLab } = userInfo;
        const labId = primaryLab._id;
        const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}/upload`; 
        const response = await fetch(url, {
            method: 'GET', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ token }`,
            }
        });
        const responseJson = await response.json();
        if(response.status == 200) return responseJson;
        else throw new ServerError({ status: response.status, ...responseJson.error });
    }
    catch(error){
        if(error.type === constants.errorTypes.ServerError) throw error;
        else throw new DeviceError({ code: 1004, message: error.message || 'Error happened when trying to get upload permission from server.' });
    }
};

// This method sends the new result to server.
const addResult = async ({ result, userInfo }) => {
    // TODO: handle errors

    try{
        const { token, primaryLab } = userInfo;
        const labId = primaryLab._id;
        const addResultUrl = `${constants.appParams.serverBaseAddress}api/labs/${labId}/results`; 
        const finalResult = { informClient: true, ...result };
        const response = await fetch(addResultUrl, {
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ token }`,
            },
            body: JSON.stringify(finalResult),
        });
        const responseJson = await response.json();
        if(response.status == 200) return responseJson.data;
        else throw new ServerError({ status: response.status, ...responseJson.error });
    }
    catch(error){
        if(error.type === constants.errorTypes.ServerError) throw error;
        else throw new DeviceError({ code: 1007, message: error.message || 'Error happened when trying submit a new result to server.' });
    }
};

/*
    This method is responsible to work with the api to request for a one-time password.
    If the request is successfull, it will resolves with the data about the otp.
    If the request is not successful, this method will reject with an error object.
    The error object will include the error data:
    {
        error: {
            code: <error code as number>,
            type: "error type as a string",
            message: "error message received from server as a string",
            moreInfo: "more info as a string",
            status: <status code as number>
        }
    } 
*/

const getOtp = async mobileNumber => {
    const getOtpUrl = `${constants.appParams.serverBaseAddress}auth/otp`;
    try{
        const response = await fetch(getOtpUrl, {
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ mobileNumber }),
        });
        const responseJson = await response.json();
        if(response.status == 200) return responseJson.data;
        else throw new ServerError({ status: response.status, ...responseJson.error });
    }
    catch(error){
        if(error.type === constants.errorTypes.ServerError) throw error;
        else throw new DeviceError({ code: 1000, message: error.message || 'Error happened when trying to get otp from server.' });
    }
};

const login = async ({ mobileNumber, otp }) => {
    const loginpUrl = `${constants.appParams.serverBaseAddress}auth/login`;
    try{
        const response = await fetch(loginpUrl, {
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ mobileNumber, otp }),
        });
        const responseJson = await response.json();
        if(response.status == 200) return responseJson;
        else throw new ServerError({ status: response.status, ...responseJson.error });
    }
    catch(error){
        if(error.type === constants.errorTypes.ServerError) throw error;
        else throw new DeviceError({ code: 1001, message: error.message || 'Error happened when trying to login.' });
    }
};


export default {
    getUploadPermission,
    addResult,
    getOtp,
    login,
};