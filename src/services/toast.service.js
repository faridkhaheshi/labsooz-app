import { Toast } from 'native-base';
import styles from '../styles/styles';

const defaultValues = {
    text: '',
    duration: 3000,
    type: null,
    position: 'top',
    textStyle: styles.toastText,
    buttonText: null,
    buttonTextStyle: {},
};

const showToast = ({
    text = defaultValues.text,
    duration = defaultValues.duration,
    type = defaultValues.type,
    position = defaultValues.position,
    textStyle = defaultValues.textStyle,
    buttonText = defaultValues.buttonText,
    buttonTextStyle = defaultValues.buttonStyle,
}) => Toast.show({
        text,
        duration,
        type,
        position,
        textStyle,
        buttonText,
        buttonTextStyle,
});

const showError = text => Toast.show({
    text,
    duration: defaultValues.duration,
    type: 'danger',
    position: defaultValues.position,
    textStyle: defaultValues.textStyle,
    buttonText: defaultValues.buttonText,
    buttonTextStyle: defaultValues.buttonStyle,
});

const showSuccess = text => Toast.show({
    text,
    duration: defaultValues.duration,
    type: 'success',
    position: defaultValues.position,
    textStyle: defaultValues.textStyle,
    buttonText: defaultValues.buttonText,
    buttonTextStyle: defaultValues.buttonStyle,
});

const showInfo = text => Toast.show({
    text,
    duration: defaultValues.duration,
    position: defaultValues.position,
    textStyle: defaultValues.textStyle,
    buttonText: defaultValues.buttonText,
    buttonTextStyle: defaultValues.buttonStyle,
});

export { showToast, showError, showSuccess, showInfo };