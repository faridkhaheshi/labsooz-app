const errorTypes = {
    ServerError: 'ServerError',
    DeviceError: 'DeviceError',
    MyError: 'MyError',
    ValidationError: 'ValidationError',
    TypeError: 'TypeError',
    UnknownError: 'UnknownError',
};

const errorCodes = {
    // codes from 100 to 999 are reserved for server errors.
    100: { toUser: 'شماره‌ی موبایلی که وارد کردید معتبر نیست. لطفا از درست بودن آن مطمئن شوید' },
    101: { toUser: 'مشکلی در سیستم ارسال پیامک وجود دارد. لطفا چند دقیقه دیگر دوباره تلاش کنید' },
    102: { toUser: 'برای ورود باید شماره موبایل معتبری ارائه کنید' },
    103: { toUser: 'رمزی که وارد کردید اشتباه است. لطفا به پیامکی که برایتان ارسال شده مراجعه کنید' },
    104: { toUser: 'به دلیل بروز مشکلی در سیستم، قادر به بررسی صحت اطلاعات شما نبودیم. لطفا بعد از چند دقیقا دوباره تلاش کنید' },
    105: { toUser: 'مشکلی پیش آمد. پوزش می‌خواهیم' },
    106: { toUser: 'شما دسترسی لازم برای این کار را ندارید' },
    107: { toUser: 'شما دسترسی لازم برای این کار را ندارید' },
    108: { toUser: 'شما دسترسی لازم را برای استفاده از این سامانه ندارید' },
    109: { toUser: 'ورودی مورد نظر به شکل درستی وارد نشده است' },
    110: { toUser: 'شما دسترسی لازم برای این کار را ندارید' },
    111: { toUser: 'آزمایشگاه مورد نظر وجود در سامانه وجود ندارد' },
    112: { toUser: 'اطلاعات لازم برای تغییر در آزمایشگاه به درستی وارد نشده است' },
    113: { toUser: 'شما به سقف تعداد کاربر آزمایشگاه‌تان رسیده‌اید. لطفا برای ارتقاء حساب کاربری‌تان با ما تماس بگیرید' },
    114: { toUser: 'به دلیل مشکلی در سیستم پیامک نتوانستیم این نتیجه را برای بیمار ارسال کنیم' },
    115: { toUser: 'اطلاعات وارد شده برای دریافت نتیجه آزمایش اشتباه است' },
    116: { toUser: 'به دلیل مشکلی در سیستم بارگزاری عکس نتوانستیم این کار را انجام دهیم. لطفا بعد از چند دقیقه دوباره تلاش کنید' },
    117: { toUser: 'اطلاعات وارد شده برای نتیجه آزمایش اشتباه است' },
    118: { toUser: 'نتیجه ی خواسته شده در سیستم موجود نیست' },
    404: { toUser: 'یافت نشد' },
    999: { toUser: 'مشکلی در سیستم پیش آمد. متاسفیم' },
    // client errors:
	1000: {
		description: 'This error happens when trying to get an otp from server fails and the reason is not serverError.',
		message: 'Unable to get otp from server.',
        moreInfo: 'Maybe no internet connection',
        toUser: 'ارتباط با سرور با مشکلی مواجه است. لطفا از وصل بودن ارتباط خود با اینترنت مطمئن شوید',
        type: errorTypes.DeviceError,
    },
    1001: {
        description: 'This error happens when trying to log in fails and the reason is not serverError.',
		message: 'Unable to login.',
        moreInfo: 'Maybe no internet connection',
        toUser: 'ورود به سیستم با مشکلی مواجه شد. لطفا ااز وصل بودن ارتباط اینترنت خود مطمئن شوید',
		type: errorTypes.DeviceError
    },
    1002: {
        description: 'This happens when trying to fetch user info from device storage.',
		message: 'Unable to fetch user info from device',
        moreInfo: 'It may be related to app permissions',
        toUser: 'مشکلی در دریافت اطلاعات شما از روی گوشی پیش آمد. لطفا مطمئن شوید که دسترسی‌های لازم را برای خواندن اطلاعات از حافظه‌ی گوشی به نرم‌افزار داده‌اید.',
		type: errorTypes.DeviceError
    },
    1003: {
        description: 'This happens at log out when trying to remove user info from device storage.',
		message: 'Unable to remove user info from device',
        moreInfo: 'It may be related to app permissions',
        toUser: 'در هنگام خروج از سیستم مشکلی پیش آمد. لطفا مطمئن شوید که دسترسی‌های لازم برای خواندن اطلاعات از حافظه‌ی گوشی به نرم‌افزار داده‌شده است.',
		type: errorTypes.DeviceError
    },
    1004: {
        description: 'This happens at when some problem other than server errors happens when trying to get an upload permission from the server.',
		message: 'Unable to get upload permission from server',
        moreInfo: 'Maybe no internet connection',
        toUser: 'مشکلی در هنگام بارگزاری عکس پیش آمد. لطفا از وصل بودن اتصال اینترنت خود مطمئن شوید',
		type: errorTypes.DeviceError
    },
    1005: {
        description: 'This happens when uploading an image to upload server (cloudinary) results in error.',
		message: 'Unable upload image to upload server',
        moreInfo: 'Probably there is some problems in the parameters of the upload form.',
        toUser: 'مشکلی در بارگزاری عکس نتیجه‌ی آزمایش پیش آمد. لطفا بعد از چند دقیقه دوباره تلاش کنید.',
		type: errorTypes.ServerError
    },
    1006: {
        description: 'This happens when uploading an image to upload server (cloudinary) is not possible due to some error.',
		message: 'Unable to upload image to upload server',
        moreInfo: 'The upload server may be down',
        toUser: 'بارگزاری عکس با مشکل مواجه شد. لطفا از وصل بودن اتصال اینترنت گوشی‌تان مطمئن شوید',
		type: errorTypes.DeviceError
    },
    1007: {
        description: 'This happens when submitting a new result server is not possible due to some error other that server errors.',
		message: 'Unable to submit result to server',
        moreInfo: 'Connection to server is not possible',
        toUser: 'اضافه کردن نتیجه‌ی آزمایش ممکن نیست. لطفا از وصل بودن اتصال اینترنت گوشی خود مطمئن شوید',
		type: errorTypes.DeviceError
    },
    1008: {
        description: 'This happens when the input for mobile number is too short',
		message: 'Length of the input string is too short to be a valid phone number',
        moreInfo: 'A valid mobile number should be at least 10 digits. excluding the country code.',
        toUser: 'مقداری که برای شماره موبایل وارد شده است کم‌تر از حد مجاز است',
		type: errorTypes.ValidationError
    },
    1009: {
        description: 'This happens when the input for mobile number is not valid',
		message: 'The input string is not a valid phone number',
        moreInfo: 'The input should be a valid combination of numbers and special characters.',
        toUser: 'مقداری که برای شماره موبایل وارد شده قابل قبول نیست',
		type: errorTypes.ValidationError
    },
    1010:{
        description: 'This happens when the the input to a method is expected to be string but it is not',
		message: 'Invalid type of input. expected a string',
        moreInfo: 'The input should be a string',
        toUser: 'ورودی در شکل مناسبی وارد نشده است',
		type: errorTypes.TypeError
    }, 
    1011:{
        description: 'This happens when the the input to the otp input is shorter than the minimum length of otp.',
		message: 'The input is too short to be a valid one-time password',
        moreInfo: 'Please enter the password sent to your mobile number',
        toUser: 'رمز وارد شده کوتاه‌تر از مقدار مجاز است',
		type: errorTypes.ValidationError
    }, 
    1012:{
        description: 'This happens when a non-numeric value is given for otp.',
		message: 'The One-time password is always a number',
        moreInfo: 'Please enter the password sent to your mobile number',
        toUser: 'رمز حتما باید یک عدد باشد',
		type: errorTypes.ValidationError
    },
    2001:{
        description: 'This happens at the splash screen when it is impossible to load the splash resources.',
		message: 'The splash resources cannot be loaded',
        moreInfo: '',
        toUser: 'مشکلی در بارگزاری فایل‌های ضروری پیش آمد',
		type: errorTypes.DeviceError
    },
    9999: {
        description: 'This is the general code for all unknow errors.',
		message: 'Unknown error',
        moreInfo: "We don't know the reason yet",
        toUser: 'مشکل نامشخصی پیش آمد.',
		type: errorTypes.UnknownError
    },
};


export {
    errorTypes,
    errorCodes,
};