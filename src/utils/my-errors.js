// Here, we define the fields that we want to have in the JSON error object.
const exportToJSON = error => {
    return {
        type: error.type,
        message: error.message,
        stackTrace: error.stack,
    };
};

class MyError extends Error {
    constructor({ code, message }){
        super(message);
        this.type = 'MyError';
        this.code = code;
    }
}

class ValidationError extends Error{
    constructor({ code, message }){
        super(message);
        this.type = 'ValidationError';
        this.code = code;
    }
}

class TypeError extends Error{
    constructor({ code, message }){
        super(message);
        this.type = 'TypeError';
        this.code = code;
    }
}

class DeviceError extends Error {
    constructor({ code, message }){
        super(message);
        this.type = 'DeviceError';
        this.message = message;
        this.code = code;
    }

    // toJSON = () => exportToJSON(this);

    // toUser = () => exportToUser(this);
}

class ServerError extends Error {
    constructor({ status, code, type, message = "", moreInfo = "" }){
        super(message);
        this.type = 'ServerError';
        this.serverType = type;
        this.code = code;
        this.status = status;
        this.moreInfo = moreInfo;
    }
}

class UnknownError extends Error {
    constructor({ code, message }){
        super(message);
        this.type = 'UnknownError';
        this.code = code;
    }
}


export { ServerError, DeviceError, MyError, UnknownError, ValidationError, TypeError };