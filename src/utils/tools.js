import api from '../services/api.service';
import constants from '../constants';
import { ServerError, UnknownError, DeviceError, ValidationError, TypeError } from './my-errors';


const romanDigitLookup = {};
romanDigitLookup[String.fromCharCode(1776)] = '0';
romanDigitLookup[String.fromCharCode(1777)] = '1';
romanDigitLookup[String.fromCharCode(1778)] = '2';
romanDigitLookup[String.fromCharCode(1779)] = '3';
romanDigitLookup[String.fromCharCode(1780)] = '4';
romanDigitLookup[String.fromCharCode(1781)] = '5';
romanDigitLookup[String.fromCharCode(1782)] = '6';
romanDigitLookup[String.fromCharCode(1783)] = '7';
romanDigitLookup[String.fromCharCode(1784)] = '8';
romanDigitLookup[String.fromCharCode(1785)] = '9';
romanDigitLookup[String.fromCharCode(1632)] = '0';
romanDigitLookup[String.fromCharCode(1633)] = '1';
romanDigitLookup[String.fromCharCode(1634)] = '2';
romanDigitLookup[String.fromCharCode(1635)] = '3';
romanDigitLookup[String.fromCharCode(1636)] = '4';
romanDigitLookup[String.fromCharCode(1637)] = '5';
romanDigitLookup[String.fromCharCode(1638)] = '6';
romanDigitLookup[String.fromCharCode(1639)] = '7';
romanDigitLookup[String.fromCharCode(1640)] = '8';
romanDigitLookup[String.fromCharCode(1641)] = '9';

const persianDigitLookup = {
    '0': String.fromCharCode(1776),
    '1': String.fromCharCode(1777),
    '2': String.fromCharCode(1778),
    '3': String.fromCharCode(1779),
    '4': String.fromCharCode(1780),
    '5': String.fromCharCode(1781),
    '6': String.fromCharCode(1782),
    '7': String.fromCharCode(1783),
    '8': String.fromCharCode(1784),
    '9': String.fromCharCode(1785),
};


// This is the promisified version of XMLHttpResuest with a callback for progress report
const requestUploadImage = (url, formData, progCB) => {
    return new Promise((resolve, reject) =>  {
        let xhr = new XMLHttpRequest();
        xhr.responseType = 'json';

        xhr.open('POST', url);
        if(progCB) xhr.upload.onprogress = progCB;
        xhr.onload = () => {
            // console.log(xhr);
            if(xhr.status == 200) resolve(xhr.response);
            else reject(new ServerError({ status: xhr.status, code: 1005, message: ((xhr.response || {}).error || {}).message||'something went wrong when trying to upload the image to upload server.' }));
            // else reject({ status: xhr.status, statusText: xhr.statusText, response: xhr.response });
        };
        xhr.onerror = error => {
            reject(new DeviceError({ code: 1006, message: error.message || 'Problem connecting to upload server.'}));
        };
        xhr.send(formData);
    });
};

// This method returns a promise resolving with the data required to be used with cloudinary upload api.
/*
    This method gets an upload permission from the server.
    If the request is successful, it will resolve with the upload data in this form:
    {
        uploadUrl: "theurl",
        formData: theFormDataObject
    }

    If the requests fails, it will reject with a ServerError with appropriate about the error.
*/
const getUploadPermission = async userInfo => {
    try{
        const { uploadUrl, formDataInfo } = await api.getUploadPermission(userInfo);

        let formData = new FormData();
        Object.keys(formDataInfo).forEach(key => formData.append(key, formDataInfo[key]));
        return { formData, uploadUrl };
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in getUploadPermission.tools'});
    }
};

// This method uploads an images and resolves with its public url.
const uploadPhoto = async ({ uri, progressCb, userInfo }) => {
    try{
        const { uploadUrl, formData } = await getUploadPermission(userInfo);
        // TODO: get the type of the file from uri and create a meaningful name for it.
        formData.append('file', {uri: uri, type: 'image/jpg', name: 'upload.jpg'});
        const result = await requestUploadImage(uploadUrl, formData, progressCb);
        return result.url;
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in uploadPhoto.tools'});
    }
};


const convertNumeralsToPersian = text => {
    if(typeof text !== 'string') throw new TypeError({code: 1010, message: 'The input should be a string'});
    const converted = text.split('').reduce((acc, c) => {
        const convertedChar = (c in persianDigitLookup)? persianDigitLookup[c]: c;
        const newAcc = `${acc}${convertedChar}`;
        return newAcc;
    }, '');
    return converted;
};

const convertNumeralsToRoman = text => {
    if(typeof text !== 'string') throw new TypeError({code: 1010, message: 'The input should be a string'});
    const converted = text.split('').reduce((acc, c) => {
        const convertedChar = (c in romanDigitLookup)? romanDigitLookup[c]: c;
        const newAcc = `${acc}${convertedChar}`;
        return newAcc;
    }, '');
    return converted;
};

const isPhoneNumber = text => {
    const pattern = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g;
    return pattern.test(text);
};

// This method converts the given phone number string to the standard format. 
// Standard format has roman letter numbers.
// If the text is not a valid phone number, returns null.
const normalizePhoneNumber = text => {
    try{
        if(text.length < 10) throw new ValidationError({ code: 1008, message: 'Length of the input string is too short to be a valid phone number' });
        
        const romanText = convertNumeralsToRoman(text);

        if(!isPhoneNumber(romanText)) throw new ValidationError({ code: 1009, message: 'The input string is not a valid phone number' });

        const normalizedText = romanText.replace(/\D/g, '').replace(/^0+/, '');
        const minLen = (/^98/g.test(normalizedText))?12:10;

        if(normalizedText.length >= minLen) return normalizedText;
        else throw new ValidationError({ code: 1008, message: 'Length of the input string is too short to be a valid phone number.' });
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in normalizePhoneNumber.tools'});
    }
};

const normalizeOtp = text => {
    if(typeof text !== 'string') throw new TypeError({code: 1010, message: 'The input should be a string'});
    if(text.length < constants.appParams.otpMinLen) throw new ValidationError({ code: 1011, message: 'Length of the input string is too short to be a valid one-time password' });

    try{
        const romanText = convertNumeralsToRoman(text); 
        if(!isNaN(romanText)) return romanText;
        else throw new ValidationError({ code: 1012, message: 'The input for otp should be numeric' });
    }
    catch(error){
        if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
        else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in normalizeOtp.tools'});
    }
};

// TODO: add more polishing (leading 0 etc.)
const polishPhoneNumber = phoneNumber =>  convertNumeralsToPersian(phoneNumber);

export default {
    photo: {
        upload: uploadPhoto
    },
    string: {
        normalizePhoneNumber,
        convertNumeralsToPersian,
        polishPhoneNumber,
        normalizeOtp
    },
};