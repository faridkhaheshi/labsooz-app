import { StyleSheet, Dimensions, Platform } from 'react-native';

const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default StyleSheet.create({
	splashContainer: {
		flex: 1,
		alignItems: 'center',
	},
	splashImage: {
		height: winHeight,
		width: winWidth,
		flex: 1,
		resizeMode: 'cover',
	},
	splashMessage:{
		position: 'absolute',
		justifyContent: 'center', 
		alignItems: 'center',
		height: 50,
		bottom: 80,
		color: 'white',
	},
	splashMessageText:{
		color: 'white', 
		textAlign: 'center'
	},
	homePageContainer: {
		flex: 1,
		alignItems: 'center',
		paddingTop: Platform.OS==='ios'? 94: 24 ,
	},
	topContainer: {
		flex: 1,
		justifyContent: 'center',
	},
	actionCenter:{
		height: 80,
		justifyContent: 'center',
	},
	bigText: {
		fontSize: 20,
		fontFamily: 'iran-sans-bold',
		textAlign: 'center',
	},
	cardImage:{
		resizeMode: 'contain',
		marginVertical: 20,
		width: winWidth*0.8,
		height: winHeight*0.4,
	},
	resultPhoto:{
		resizeMode: 'contain',
		marginBottom: 20,
		width: winWidth*0.8,
		height: winHeight*0.4,
	},
	generalTextInput: {
		fontFamily: 'iran-sans',
		fontSize: 14,
		textAlign: 'center',
	},
	headerButton: {
		paddingHorizontal: 20,
	},
	toastText:{
		fontSize: 12,
		fontFamily: 'iran-sans',
		writingDirection: 'rtl',
		textAlign: 'justify',
	},
});