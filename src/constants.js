import { errorTypes, errorCodes } from './utils/errors';

const appStates = {
    home: {
        noPhoto: 'NO_PHOTO',
        waitingForPhoneNumber: 'WAITING_FOR_PHONE_NUMBER',
        readyToSend: 'READY_TO_SEND',
        pictureUploaded: 'PICTURE_UPLOADING',
        pictureUploaded: 'PICTURE_UPLOADED',
        resultAdded: 'RESULT_ADDED',
        noInternet: 'NO_INTERNET',
        error: 'ERROR',
    },
};
const appTexts = {
    appName: 'لب‌سوز',
    home: {
        title: 'خانه',
        noPhotoGuide: 'اگر نتیجه‌ی آزمایش بیمار آماده شده، از آن عکس بگیرید تا برای بیمار ارسال شود.',
        goToCameraButton: 'عکس گرفتن',
        phoneNumberPlaceholder: 'مثلا ۰۹۳۰۱۲۳۴۵۶۷',
        confirmPhoneNumber: 'تایید شماره موبایل',
    },
    camera: {
        givePermissionButton: 'دادن دسترسی به دوربین',
        givePermissionGuide: 'برای استفاده از این امکان باید اجازه‌ی دسترسی به دوربین را به نرم‌افزار بدهید',
    },
    resultForm: {
        title: 'شماره موبایل',
        enterPhoneNumberGuide: 'شماره‌ی تلفن همراه بیمار را وارد کنید.',
        imageDescription: 'این عکس برای بیمار ارسال خواهد شد.',
    },
    confirmResult: {
        title: 'تایید نهایی',
        uploadStartMessage: 'در حال بارگذاری عکس...',
        uploadFinishedMessage: 'بارگذاری عکس با موفقیت انجام شد',
        submittingResulttMessage: 'در حال اضافه کردن نتیجه‌ی آزمایش',
        resultAddedMessage: 'نتیجه‌ی آزمایش با موفقیت ثبت شد. الآن به صفحه‌ی اول باز می‌گردید.',
        button: 'تایید',
        guide: 'این نتیجه‌ی آزمایش برای شماره‌ی زیر ارسال خواهد شد. تایید می‌کنید؟',
    },
    login: {
        confirmOtp: 'ورود',
        confirmPhoneNumber: 'تایید شماره موبایل',
        getOtpTitle: 'ورود',
        loginTitle: 'وارد کردن رمز',
        getOtpGuide: 'برای ورود شماره موبایل خود را وارد کنید:',
        loginGuide: 'رمزی را که برای شماره موبایل‌تان ارسال شده وارد کنید:',
        otpPlaceholder: 'رمز',
    },
    general: {
        phoneNumberPlaceholder: 'مثلا ۰۹۳۰۱۲۳۴۵۶۷',
    },
};

const timing = {
    minSplash: 1000,
    goToNextResult: 1000,
};

const appParams = {
    phoneNumberMaxLen: 15,
    otpMaxLen: 8,
    otpMinLen: 4,
    serverBaseAddress: 'https://labsooz.herokuapp.com/',
    localStorageKey: 'USER_INFO',
    actionCenterHeight: 80,
};

export default { appStates, appTexts, timing, appParams, errorTypes, errorCodes };