import React from 'react';
import { 
    Header, 
    Body, 
    Left, 
    Title, 
    Right,
    Button, 
    Icon,
} from 'native-base';

import TopMenu from './top-menu.component';
import constants from '../constants';

export default ({
    title = constants.appTexts.appName,
    appActions,
    navigation,
    noMenu = null,
    hasClose = null,
}) => (
        <Header>
            <Left>{ hasClose && (
                <Button transparent onPress={ () => navigation.navigate('Home') }>
                    <Icon name='close'/>    
                </Button>
                )}</Left>
            <Body>
                <Title>{ title }</Title>
            </Body>
            <Right>
                {
                    (!noMenu) && <TopMenu appActions={ appActions } navigation={ navigation } />
                }
            </Right>
        </Header>
);