import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import tools from '../utils/tools';
import styles from '../styles/styles';
import constants from '../constants';


export default class PhoneNumberInput extends React.Component {
    state = {
        isValid: false,
    };

    _handleChangeText = text => {
        const normalizedPhoneNumber = tools.string.normalizePhoneNumber(text);
        if(normalizedPhoneNumber) {
            this.props.onValidInput(normalizedPhoneNumber);
            if(!this.state.isValid) this.props.onStatusChange(true);
            this.setState({ isValid: true });
        }
        else{
            if(this.state.isValid) this.props.onStatusChange(false);
            this.setState({ isValid: false });
        };
    };

    render = () => (
    <TextInput 
        style={[ styles.generalTextInput, this.props.style ]}
        onChangeText={ this.props.onChangeText || this._handleChangeText }
        autoFocus={ true }
        maxLength={ this.props.maxLength || constants.appParams.phoneNumberMaxLen }
        placeholder={ this.props.placeholder || constants.appTexts.home.phoneNumberPlaceholder }
        keyboardType='numeric'
    />);
};
