import React from 'react';
import { Image } from 'react-native';
import { 
    Card,
    CardItem,
    Body,
    Text,
    Item,
    Icon,
    Input,
} from 'native-base';

import styles from '../styles/styles';
import constants from '../constants';

// const createBodyItem = (bodyItem, key, props) => {
//     return(<Text>body item<Text>);
// }
const createBodyItem = (type, key, {
    text='',
    image=null,
    getUpdates=null,
    formSuccess=null,
    autoFocus=false,
    placeholder='',
    keyboardType=null,
    maxLength=null,
    textStyle={}
}) => {
    switch(type){
        case 'text':
            return(<Text key={key} style={ textStyle }>{text}</Text>);
            break;
        case 'image':
            return(<Image key={key} style={ styles.cardImage } source={ image } />);
            break;
        case 'mobile':
            return(
                <Item key={key} success={ formSuccess }>
                    <Icon active type="Entypo" name="old-mobile" />
                    <Input style={ styles.generalTextInput }
                        placeholder={ constants.appTexts.general.phoneNumberPlaceholder } 
                        keyboardType='numeric'
                        onChangeText={ getUpdates }
                        autoFocus={ autoFocus }
                        maxLength={ constants.appParams.phoneNumberMaxLen }
                        />
                </Item>
            );
            break;
        case 'input':
            return(
                <Item key={key} success={ formSuccess }>
                    <Input style={ styles.generalTextInput }
                        placeholder={ placeholder } 
                        keyboardType={ keyboardType }
                        onChangeText={ getUpdates }
                        autoFocus={ autoFocus }
                        maxLength={ maxLength }
                        />
                </Item>
            );
            break;
        default:
            return(<Text key={key}>{`Unhandled cardItem type ${type}`}</Text>);
    }
};

export default MyCardItem = (props) => (
    <CardItem bordered={ props.bordered }><Body style={ props.bodyStyle }>{
        props.type.split('-').map((itemType, i) => createBodyItem(itemType, i, props))
    }</Body></CardItem>
);