import React from 'react';
import { Image, View, StyleSheet } from 'react-native';
import * as Progress from 'react-native-progress';
import tools from '../utils/tools';
import constants from '../constants';


export default class ImageUploader extends React.Component {
    state = {
        uploadStarted: false,
        uploading: false,
        progress: 0,
    };

    // This method updates the upload progress state by receiving the progress from xhr request.
    updateUploadProgress = (evt) => {
        if (evt.lengthComputable) {
            let percentComplete = (evt.loaded / evt.total);  
            this.setState({ progress: percentComplete });
        } 
    };

    resetProgress = () => this.setState({ progress: 0 });

    upload = async () => {
        try{
            this.setState({ uploadStarted: true, uploading: true });
            const publicUrl = await tools.photo.upload({
                uri: this.props.localPhotoUri,
                progressCb: this.updateUploadProgress,
                userInfo: this.props.userInfo,
            });
            this.setState({ uploading: false });
            return publicUrl;
            // this.props.onFinish(publicUrl);
        }
        catch(error){
            if(Object.keys(constants.errorTypes).includes(error.type) ) throw error;
            else throw new UnknownError({ code: 9999, message: error.message || 'something unknown caused problem in upload.image-uploader.component'});
        }
    }

    render(){
        return (
            <View style={ componentStyles.container }>
                <Image style={ [ componentStyles.image , this.props.style, { opacity: (this.state.uploading)?0.5:1} ] }
                    source={{ uri: this.props.localPhotoUri }}/>
            
                {
                    this.state.uploading?
                    (<View style={ componentStyles.overlay }>
                        <Progress.Pie progress={ this.state.progress } size = { 50 } color='white' borderWidth={ 0 } />    
                    </View>)
                    : null
                }
            </View>
        );
    }
};

const componentStyles = StyleSheet.create({
    container: { 
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    image: { },
    overlay: {
        position: 'absolute',
        opacity: 1,
    }, 
});