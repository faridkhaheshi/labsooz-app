import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        minHeight: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        backgroundColor: '#2AC062',
    },
    disabled: {
        backgroundColor: '#eeeeee',
    },
    text: {
        fontFamily: 'iran-sans',
        fontSize: 16,
        color: '#FFFFFF',
    },
    disabledText: {
        color: '#bbbbbb',
    },
});

export default ({
    title= 'button',
    style= {},
    textStyle= {},
    disabled= false,
    disabledStyle= {},
    disabledTextStyle= {},
    onPress
}) => (
    <TouchableOpacity 
        onPress={ !disabled? onPress: null } 
        style={
            !disabled?
            [ styles.button, style ]
            :[styles.button, styles.disabled, style, disabledStyle ]
        }
        disabled={disabled} 
        >
        <Text 
            style={
                !disabled? 
                [styles.text, textStyle]
                :[styles.text, styles.disabledText, textStyle, disabledTextStyle ] 
                }
        >{ title }</Text>
    </TouchableOpacity>
);