import React from 'react';
import { Icon, Button } from 'native-base';
import styles from '../styles/styles';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';


const triggerStyles = {
    triggerText: {},
    triggerOuterWrapper: { flex: 1 },
    triggerWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    triggerTouchable: {
        activeOpacity: 70,
        style : { flex: 1 }
    },
};

const optionsStyles = {
    optionsContainer: { padding: 5 },
    optionsWrapper: {},
    optionWrapper: { 
        margin: 5,
        minHeight: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionTouchable: {
        underlayColor: '#f5b642',
        activeOpacity: 70,
    },
    optionText: { 
        fontFamily: 'iran-sans',
        fontSize: 14,
        textAlign: 'center'
    },
};




export default class TopMenu extends React.Component{

    constructor(props){
        super(props);

        this._handleOptions = this._handleOptions.bind(this);
    }

    _handleOptions = value => {
        switch(value){
            case 1:
                this.props.navigation.navigate('Home');
                break;
            case 2:
                this._handleLogOut();
                break;
            default:
                return;
        }
    };

    _handleLogOut = async () => {
        try{
            await this.props.appActions.logout();
            this.props.navigation.navigate('Auth');
        }
        catch(error){
            // TODO: handle logout errors.
            console.log(error);
        }
    };

    openMenu = () => this.menu.open();

    onRef = r =>    this.menu = r;

    render = () => (
        <Menu onSelect={ this._handleOptions } ref={ this.onRef }>
            <MenuTrigger customStyles={triggerStyles}>
                <Button transparent onPress={ this.openMenu }>
                    <Icon name='more'/>    
                </Button>
            </MenuTrigger>
            <MenuOptions customStyles={optionsStyles}>
                <MenuOption value={1} text="صفحه اصلی"/>
                <MenuOption value={2} text="خروج از حساب کاربری"/>
            </MenuOptions>
        </Menu>
    );
};