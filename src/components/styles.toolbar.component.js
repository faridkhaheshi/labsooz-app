import { StyleSheet, Dimensions } from 'react-native';
const { width: winWidth } = Dimensions.get('window');

export default StyleSheet.create({
    bottomToolbar: {
		width: winWidth,
		position: 'absolute',
		height: 100,
		bottom: 0,
    },
    alignCenter: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
    },
    captureBtn: {
		width: 60,
		height: 60,
		borderWidth: 2,
		borderRadius: 60,
		borderColor: "#FFFFFF",
    },
    captureBtnActive: {
		backgroundColor: "#FFFFFF",
	},
});