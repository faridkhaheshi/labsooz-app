import React from 'react';
import { View } from 'react-native';
import { 
    Spinner,
    Text,
    Button,
} from 'native-base';
import constants from '../constants';


export default ({
    containerStyle = {},
    text = 'Button',
    onPress = null,
    disabled = false,
    processing = false,
}) => (
        <View style={[ { height: constants.appParams.actionCenterHeight , justifyContent: 'center'}, containerStyle ]}>
            <Button block
                onPress = { onPress }
                disabled = { disabled }
                transparent={ processing }
                >
                {
                    (!processing)?
                    <Text> { text } </Text>:
                    <Spinner />
                }
            </Button>
        </View>
);