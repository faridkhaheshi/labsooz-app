import React from 'react';
import { Platform, StatusBar } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import AppLoadingPage from '../pages/app-loading.page';
import AppNavigator from './app.navigator';
import AuthNavigator from './auth.navigator';

const topNavigator = createSwitchNavigator(
    {
        AppLoading: AppLoadingPage,
        App: AppNavigator,
        Auth: AuthNavigator,
    },
    {
        initialRouteName: 'AppLoading',
    }
)

export default createAppContainer(topNavigator);
