import React from 'react';
import { Platform, StatusBar } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import CameraPage from '../pages/camera.page';
import HomePage from '../pages/home.page';
import ResultFormPage from '../pages/result-form.page';
import ConfirmResultPage from '../pages/confirm-result.page';

export default createStackNavigator({
    Home: { screen: props => <HomePage { ...props }/> },
    Camera: { screen: props => <CameraPage { ...props }/> },
    ResultForm: { screen: props => <ResultFormPage { ...props }/> },
    ConfirmResult: { screen: props => <ConfirmResultPage { ...props }/> },
},
    {
        initialRouteName: "Home",
        cardStyle: {
            paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
        },
        defaultNavigationOptions: {
            header: null,
            gestureDirection: 'inverted',
        },
    }
);