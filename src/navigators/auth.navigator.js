import { Platform, StatusBar } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import GetOtpPage from '../pages/get-otp.page';
import LoginPage from '../pages/log-in.page';

export default createStackNavigator({
    GetOtp: { screen: GetOtpPage },
    Login: { screen: LoginPage },
},
    {
        initialRouteName: "GetOtp",
        cardStyle: {
            paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
        },
        defaultNavigationOptions: {
            header: null,
            gestureDirection: 'inverted',
        },
    }
);